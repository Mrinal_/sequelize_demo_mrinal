const Sequelize = require("sequelize")
const connection = require("./Connection")

const User = connection.define("userData", {
    name:{
        type: Sequelize.STRING(),
        allowNull: false
    },
    email:{
        type: Sequelize.STRING(),
        allowNull: false
    },
})

module.exports = User