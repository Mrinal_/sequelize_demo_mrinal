const dotenv = require("dotenv").config()

module.exports={
    port     : process.env.PORT || 3000,
    host     : process.env.host,
    user     : process.env.user,
    database : process.env.database,
    password : process.env.password,
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
}