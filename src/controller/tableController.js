const User = require("../models/User");

// Create and Save a new row
const create = () => {
    const user = {
        name: "mrinal",
        email:"abc@gmail.com"
    } 
    User.create(user)
        .catch((error)=>{
            console.log(error)
        })
}

// Retrieve all rows from the database.
const findAll = () => {
    User.findAll()
    .then((data)=>{
        console.log(data)
    })
    .catch((error)=>{
        console.log(error)
    })
}

// Find a single row with an id
const findOne = () => {
    const id = 2

    User.findByPk(id)
        .then((data)=>{
            if(data == null){
                console.log(`No data available with id ${id}`)
            }
            else{
                console.log(data)
            }
        })
        .catch((error)=>{
            console.log(error)
        })
}

// Update a row by the id in the request
const update = () => {
    const id = 1
    const user = {
        email:"xyz@gmail.com"
    } 

    User.update(user, {
        where: {
            id: id
        }
    })
    .then((data)=>{
        if(data == 1){
            console.log("Data updated")
        }
        else{
            console.log(`Cannot update data with id ${id}`)
        }
    })
    .catch((error)=>{
        console.log(error)
    })
}

// Delete a row with the specified id in the request
const remove = () => {
    const id = 2

    User.destroy({
        where:{
            id: id
        }
    })
    .then((data)=>{
        if(data == 1){
            console.log("Data deleted") 
        }
        else{
            console.log(`Cannot delete data with id ${id}`)
        }
    })
    .catch((error)=>{
        console.log(error)
    })
}

// Delete all rows from the database.
const removeAll = () => {

    User.destroy({
        where: {},
        truncate : false
    })
    .then((data)=>{
        console.log(`${data} records deleted successfully`)
    })
    .catch((error)=>{
        console.log(error)
    })
}


module.exports = {
    create,
    findAll,
    findOne,
    update,
    remove,
    removeAll,
}